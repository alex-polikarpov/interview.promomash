# Cкриншоты #

**Главная страница, первый запуск**

![1.png](https://gitlab.com/23Alex24/interview.promomash/raw/master/images/1.png)

**Front-end валидация**

![4.png](https://gitlab.com/23Alex24/interview.promomash/raw/master/images/4.png)

**При переходе между страницами loader**

![5.png](https://gitlab.com/23Alex24/interview.promomash/raw/master/images/5.png)

**Шаг 2**

![6.png](https://gitlab.com/23Alex24/interview.promomash/raw/master/images/6.png)

**Валидация шага 2**

![7.png](https://gitlab.com/23Alex24/interview.promomash/raw/master/images/7.png)

**Шаг 3 (сообщение об успешной регистрации)**. При нажатии на кнопку "Register new user" переход на шаг

![8.png](https://gitlab.com/23Alex24/interview.promomash/raw/master/images/8.png)

**Серверные ошибки, попытка зарегистрировать пользователя с используемым email-ом**

![9.png](https://gitlab.com/23Alex24/interview.promomash/raw/master/images/9.png)

**Открываем 2 окна, из обоих окон вводим second@test.test и переходим к шагу 2**. Дальше из одного окна заканчиваем регистрацию пользователя, а из второго окна пытаемся ее завершить. Нас перекидывает снова к шагу №1 с ошибкой, что second@test.test уже используется. Обработка данной ситуации важна, потому что в реальном приложении она может произойти. При переходе от шага 1 к шагу 2 мы делаем запрос к серверу на проверку используется ли почта, она есть и при попытке зарегистрироваться на втором шаге, НО на втором шаге нет кнопки "Назад к шагу 1", поэтому мы перенаправляем пользователя на шаг 1 для смены почты. Конечно если бы была кнопка "Назад к шагу 1" пользователь смогу бы самостоятельно перейти к шагу 1 и сменить почту, но для этого ему пришлось бы делать доп. действие.

![10.png](https://gitlab.com/23Alex24/interview.promomash/raw/master/images/10.png)