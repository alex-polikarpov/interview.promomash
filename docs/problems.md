# Возникшие проблемы при работе над заданием, их решения #

## **Ошибка при попытке создать миграцию** ##


`
Unable to create an object of type 'PromomashContext'. Add an implementation of 
'IDesignTimeDbContextFactory<PromomashContext>' to the project, or see https://go.microsoft.com/fwlink/?
linkid=851728 for additional patterns supported at design time.
`

Ошибка возникала из-за того, что в Program.cs классе не было метода BuildWebHost который по умолчанию в ASP.NET core вызывается. При вызове создания миграции ищется именно метод BuildWebHost в Program.cs классе, если его нет вылетают подобные ошибки.

## **Не создавалась миграция** ##

`
erationException: Unable to resolve service for type 'Microsoft.EntityFrameworkCore.Migrations.IMigrator'. This is
 often because no database provider has been configured for this DbContext. A provider can be configured by overriding
 the DbContext.OnConfiguring method or by using AddDbContext on the application service provider. If AddDbContext is
used, then also ensure that your DbContext type accepts a DbContextOptions<TContext> object in its constructor and 
passes it to the base constructor for DbContext.
`

Ошибка возникала когда пытались создать миграцию при настройке на in-memory провайдер. Нужно было переключиться на конкретную базу, миграции могут создаваться только для них.

## **Не выполнялся seed метод для in-memory провайдера** ##
Для in-memory провайдера и базы нужны были разные настройки при запуске:


```
if (!isMemory)
{
    context.Database.Migrate();
}
else
{
     context.Database.EnsureCreated();
}
```

## **Зависает консоль при вызове команды ng new** ##

Зависало из-за того, что при создании нового проекта через Angular cli устанавливаются также и пакеты и из-за них иногда зависало. Решение: вызывать вместе с опцией  --skip-install, а затем отдельно вызывать npm install  для восстановления npm пакетов

## **При билде проекта вылетают ошибки про Node.js** ##

Может возникать если не восстановлены некоторые пакет. Для решения прописали Task в .csproj файле, чтобы перед запуском восстанавливались пакеты.

`
<Target Name="PreBuild" BeforeTargets="PreBuildEvent">
    <Exec Command="npm install" WorkingDirectory="ClientApp" />
  </Target>
`

## **При старте проекта не может найти Angular Cli** ##

Возникает если локально не установлен Angular cli. Он должен быть установлен не только глобально, но и в самой директории front-end приложения. Скрин ошибки ниже 

![12.png](https://gitlab.com/23Alex24/interview.promomash/raw/master/images/12.png)

## **После установки пакета ngx-spinner не работает front-end ** ##

Хотя в документации к пакету и было указано, что он поддерживает Angular 5, по факту вылетала ошибка. Пришлось искать другой лоадер.

`
node_modules/ngx-spinner/fesm5/ngx-spinner.js
69:59-75 "export 'defineInjectable' was not found in '@angular/core'
 @ ./node_modules/ngx-spinner/fesm5/ngx-spinner.js
`

## **Проблема c Observable и catch** ##

При вызове catch над Observable похоже генерировался новый Observable. При написании утилит для обращения к api на фронте я хотел сделать базовый класс апи, который бы возвращал Observable и использующий код на него подписывался, НО я хотел также обрабатывать все ошибки и возвращать из базового api объект Response, потому что помимо серверных ошибок типа 500, 404 могли быть также и клиентские, чтобы вызывающему коду не нужно было обрабатывать в нескольких местах ошибки а можно было подписаться на успешный результат и уже внутри этого обработчика смотреть на errorCode.

Изначально возврат Observable был следующий (из api):

```
var result = this._http.get<T>(requestUrl);
    result.catch(error => {
      return Observable.of(empty);
    })
.subscribe(
      val => console.log('Value emitted successfully', val),
      error => {
        console.error("This line is never called ", error);
      },
      () => console.log("HTTP Observable completed...")
    );;
return result;
```

Код вызывал api так:

```
var result = this._http.get<T>(requestUrl);
    result.subscribe(data => data, error => {
      console.log("ERRRORRRRRR");
      return Observable.of(empty);
    });
```

При вызове в консоли видели:
"ERRRORRRRRR" - т.е. в api catch не срабатывал. 

Причина такого поведения - я не нашел доказательства, но похоже при вызове метода catch создавался новый Observable, а возвращали мы другой объект. Т.е. если из базового апи возвращать 

```
return this._http.get<T>(requestUrl).catch(error => {
      return Observable.of(empty);
    });
```

то код вызова уже работал корректно и так как задумывалось. 
P.s. возвращать Observable было не совсем правильно и в конечном варианте возвращался Promise.