﻿using System;

namespace Interview.Promomash.Core.Entities
{
    /// <summary>
    /// User entity
    /// </summary>
    public class User : IEntity
    {
        public const int EMAIL_MAX_LENGTH = 150;
        public const int EMAIL_MIN_LENGTH = 5;
        public const int PASSWORD_MIN_LENGTH = 8;
        public const int PASSWORD_MAX_LENGTH = 100;


        public Guid Id { get; set; }

        public string Email { get; set; }

        /// <summary>
        /// Hash code of user password
        /// </summary>
        public string PasswordHash { get; set; }

        /// <summary>
        /// Salt for calculating hash code
        /// </summary>
        public string Salt { get; set; }

        public int CityId { get; set; }


        /// <summary>
        /// Navigation property
        /// </summary>
        internal City City { get; set; }
    }
}
