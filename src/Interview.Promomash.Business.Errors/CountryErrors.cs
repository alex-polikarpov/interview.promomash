﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interview.Promomash.Business.Errors
{
    public enum CountryErrors
    {
        /// <summary>
        /// Undefined error
        /// </summary>
        Undefined = 0,

        CountryNotFound = 100001,

        CityNotFound = 100002
    }
}
