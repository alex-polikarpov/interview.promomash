﻿namespace Interview.Promomash.Business.Errors
{
    public static class CountryErrorsExtensions
    {
        public static string GetErrorMessage(this CountryErrors error)
        {
            switch (error)
            {
                case CountryErrors.CountryNotFound:
                    return "Country not found";

                case CountryErrors.CityNotFound:
                    return "City not found";
                    
                default:
                    return string.Empty;
            }
        }
    }
}
