﻿namespace Interview.Promomash.Business.Errors
{
    public static class SystemErrorsExtensions
    {
        public static string GetErrorMessage(this SystemErrors value)
        {
            switch (value)
            {
                case SystemErrors.BadRequest:
                    return "Bad request";

                case SystemErrors.InternalServerError:
                    return "Internal server error";

                default:
                    return string.Empty;
            }
        }
    }
}
