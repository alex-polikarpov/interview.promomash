﻿namespace Interview.Promomash.Business.Errors
{
    public static class UserErrorsExtensions
    {
        public static string GetErrorMessage(this UserErrors value)
        {
            switch (value)
            {
                case UserErrors.EmailAlreadyUsed:
                    return "Email already used";

                default:
                    return string.Empty;
            }
        }
    }
}
