﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interview.Promomash.Business.Commands
{
    public class RegisterUserCommandArgs : BaseCommandArguments
    {       
        public string Email { get; set; }
        
        public string Password { get; set; }
       
        public int CityId { get; set; }        
    }
}
