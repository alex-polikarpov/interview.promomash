﻿using System;
using System.Threading.Tasks;
using Interview.Promomash.Business.Errors;
using Interview.Promomash.Common.Utils;
using Interview.Promomash.Core.Entities;
using Interview.Promomash.Core.Services;

namespace Interview.Promomash.Business.Commands
{
    internal class RegisterUserCommand : BaseCommand<RegisterUserCommandArgs, EmptyCommandResult>
    {
        private readonly IUserService _userService;
        private readonly ICountryService _countryService;
        private readonly ICryptographyUtility _cryptographyUtility;
        private readonly IUnitOfWork _unitOfWork;

        public RegisterUserCommand(IUserService userService, ICountryService countryService, 
            ICryptographyUtility cryptographyUtility, IUnitOfWork unitOfWork)
        {
            this._userService = userService;
            this._countryService = countryService;
            _cryptographyUtility = cryptographyUtility;
            this._unitOfWork = unitOfWork;
        }

        protected override async Task<EmptyCommandResult> InternalExecute(RegisterUserCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};

            var newUser = new User
            {
                CityId = arguments.CityId,
                Email = arguments.Email.ToLowerInvariant(),
                Salt = Guid.NewGuid().ToString("N").ToUpperInvariant()
            };

            newUser.PasswordHash =
                _cryptographyUtility.ComputeHashCode(HashCodeAlgorithm.Sha512, arguments.Password, newUser.Salt).ToUpperInvariant();

            var repo = _unitOfWork.GetRepository<User>();
            repo.Add(newUser);
            await _unitOfWork.SaveChangesAsync();
            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(RegisterUserCommandArgs arguments)
        {
            var result = new EmptyCommandResult(){IsSuccess = true};

            if (await _userService.EmailIsUsed(arguments.Email))
            {
                return result.AddError(UserErrors.EmailAlreadyUsed.GetErrorMessage(),
                    (int) UserErrors.EmailAlreadyUsed);
            }

            if (!await _countryService.CityIsExists(arguments.CityId))
                return result.AddError(CountryErrors.CityNotFound.GetErrorMessage(), (int) CountryErrors.CityNotFound);

            return result;
        }
    }
}
