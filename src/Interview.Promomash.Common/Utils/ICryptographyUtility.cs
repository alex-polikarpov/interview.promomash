﻿namespace Interview.Promomash.Common.Utils
{
    /// <summary>
    /// Utility for cryptography
    /// </summary>
    public interface ICryptographyUtility
    {
        /// <summary>
        /// Returns hash code for value
        /// </summary>
        /// <param name="algorithm">Hashing algorithm</param>
        /// <param name="value">Hashing value</param>
        string ComputeHashCode(HashCodeAlgorithm algorithm, string value);

        /// <summary>
        /// Returns hash code with salt for value 
        /// </summary>
        /// <param name="algorithm">Hashing algorithm</param>
        /// <param name="value">Hashing value</param>
        /// <param name="salt">Salt for hashing</param>
        string ComputeHashCode(HashCodeAlgorithm algorithm, string value, string salt);
    }
}
