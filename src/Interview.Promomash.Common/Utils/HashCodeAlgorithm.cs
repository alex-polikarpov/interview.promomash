﻿namespace Interview.Promomash.Common.Utils
{
    public enum HashCodeAlgorithm
    {
        Md5 = 0,

        Sha256 = 1,

        Sha512 = 2
    }
}
