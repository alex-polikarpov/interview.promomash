﻿namespace Interview.Promomash.Common
{
    public static class RegexConstants
    {
        public const string PASSWORD = @"^\d+[a-zA-Z]+[a-zA-Z0-9]*|[a-zA-Z]+\d+[a-zA-Z0-9]*$";

        public const string EMAIL = "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+[.][a-zA-Z0-9-]+$";
    }
}
