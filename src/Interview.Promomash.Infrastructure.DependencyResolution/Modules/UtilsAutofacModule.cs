﻿using Autofac;
using Interview.Promomash.Common.Utils;
using Interview.Promomash.Infrastructure.Utils.Cryprography;

namespace Interview.Promomash.Infrastructure.DependencyResolution
{
    public class UtilsAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CryptographyUtility>().As<ICryptographyUtility>();
        }
    }
}
