﻿using System.Linq;
using Autofac;
using Interview.Promomash.Core.Services;
using Interview.Promomash.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using AutofacModule = Autofac.Module;

namespace Interview.Promomash.Infrastructure.DependencyResolution
{
    /// <summary>
    /// Autofac module for registration services and UnitOfWork
    /// </summary>
    public class ServicesAutofacModule : AutofacModule
    {
        private readonly DbContextOptions<PromomashContext> _options;

        public ServicesAutofacModule(DbContextOptions<PromomashContext> options)
        {
            _options = options;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(x => new PromomashContext(_options)).AsSelf().As<DbContext>().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(EfRepository<>)).As(typeof(Repository<>));
            builder.RegisterType<EfUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();

            RegisterServices(builder);
        }

        private void RegisterServices(ContainerBuilder builder)
        {
            var baseServiceType = typeof(BaseService);

            var servicesTypes = baseServiceType.Assembly
                .GetTypes()
                .Where(x => x.IsSubclassOf(baseServiceType) && !x.IsAbstract)
                .ToList();

            foreach (var service in servicesTypes)
            {
                var serviceInterface = service.GetInterfaces().Single();
                builder.RegisterType(service).As(serviceInterface);
            }
        }
    }
}
