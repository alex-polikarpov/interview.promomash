﻿using Interview.Promomash.Responses.Base;

namespace Interview.Promomash.Responses.User
{
    public sealed class CheckEmailResponse : BaseResponse
    {
        /// <summary>
        /// True if email already used
        /// </summary>
        public bool AlreadyUsed { get; set; }
    }
}
