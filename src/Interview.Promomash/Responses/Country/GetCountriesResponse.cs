﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interview.Promomash.Responses.Base;

namespace Interview.Promomash.Responses.Country
{
    public class GetCountriesResponse : BaseResponse
    {
        public Core.Entities.Country[] Countries { get; set; }
    }
}
