﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interview.Promomash.Core.Entities;
using Interview.Promomash.Responses.Base;

namespace Interview.Promomash.Responses.Country
{
    public class GetCititesResponse : BaseResponse
    {
        public City[] Cities { get; set; }
    }
}
