﻿namespace Interview.Promomash.Responses.Base
{
    /// <summary>
    /// Base class for responses from controller (for api)
    /// </summary>
    public abstract class BaseResponse
    {
        public virtual string ErrorMessage { get; set; }

        /// <summary>
        /// Its not http code. Its a custom code for business operations
        /// </summary>
        public virtual int ErrorCode { get; set; }
    }
}
