﻿using Autofac;
using Interview.Promomash.Infrastructure.Data;
using Interview.Promomash.Infrastructure.DependencyResolution;
using Microsoft.EntityFrameworkCore;

namespace Interview.Promomash.AppStart
{
    internal static class DependencyConfig
    {
        public static void Configure(ContainerBuilder containerBuilder, DbContextOptions<PromomashContext> options)
        {
            containerBuilder.RegisterModule(new ServicesAutofacModule(options));
            containerBuilder.RegisterModule(new UtilsAutofacModule());
            containerBuilder.RegisterModule(new BusinessModule());           
        }
    }
}
