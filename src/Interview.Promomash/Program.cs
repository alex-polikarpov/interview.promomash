﻿using System.Text.RegularExpressions;
using Autofac.Extensions.DependencyInjection;
using Interview.Promomash.Common;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Interview.Promomash
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var webHost = BuildWebHost(args);
            webHost.Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .ConfigureServices(services => services.AddAutofac())
                .Build();
        }
    }
}
