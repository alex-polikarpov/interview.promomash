import { RegexConstants } from '../../constants/regex.contants';
import { ValidationResult } from '../../utils/validation/validation-result.utils';
import { ErrorsConstants } from "../../constants/errors.constants";
import { CityEntity } from "../../entities/city.entity";
import { CountryEntity } from "../../entities/country.entity";

export class RegistrationStep2Model {
  public countries: CountryEntity[];
  public cities: CityEntity[];
  public countryId: number;
  public cityId: number;

  constructor(countries: CountryEntity[]) {
    this.countries = countries;
    this.countryId = 0;
    this.cityId = 0;
  }



  public resetCities() {
    this.cityId = 0;
    this.cities = null;    
  }

  public isValid(): boolean {
    return this.countryId > 0 && this.cityId > 0;
  }

  public clear() {
    this.resetCities();
    this.countryId = 0;
  }
}
