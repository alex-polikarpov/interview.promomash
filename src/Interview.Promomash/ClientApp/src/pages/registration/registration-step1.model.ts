import { RegexConstants } from '../../constants/regex.contants';
import { ValidationResult } from '../../utils/validation/validation-result.utils';
import { ErrorsConstants } from "../../constants/errors.constants";

export class RegistrationStep1Model {
  public readonly emailMinLength: number = 5;
  public readonly emailMaxLength: number = 150;
  public readonly passwordMinLength: number = 8;
  public readonly passwordMaxLength: number = 100;

  
 
  public email: string;

  public password: string;

  public confirmPassword: string;

  public iAgree: boolean;  



  public validateEmail(): ValidationResult {
    if (!this.email)
      return new ValidationResult(false, ErrorsConstants.required);

    if (this.email.length > this.emailMaxLength)
      return new ValidationResult(false, ErrorsConstants.maxLength(this.emailMaxLength));

    if (!RegexConstants.email.test(this.email))
      return new ValidationResult(false, "Enter valid email address");
    
    return new ValidationResult(true, '');
  }


  public validatePassword(): ValidationResult {
    if (!this.password)
      return new ValidationResult(false, ErrorsConstants.required);

    if (this.password.length < this.passwordMinLength)
      return new ValidationResult(false, ErrorsConstants.minLength(this.passwordMinLength));

    if (this.password.length > this.passwordMaxLength)
      return new ValidationResult(false, ErrorsConstants.maxLength(this.passwordMaxLength));

    if (!RegexConstants.password.test(this.password))
      return new ValidationResult(false, "Enter valid password");

    return new ValidationResult(true, '');
  }


  public validateConfirmPassword(): ValidationResult {
    if (!this.confirmPassword)
      return new ValidationResult(false, ErrorsConstants.required);

    if (this.confirmPassword !== this.password)
      return new ValidationResult(false, "Enter valid confirm password");

    return new ValidationResult(true, '');
  }

  public clear() {
    this.email = "";
    this.password = "";
    this.confirmPassword = "";
    this.iAgree = false;
  }
}
