export class CityEntity {

  public id: number;
  public title: string;
  public countryId: number;
}
