export class ValidationResult {

  constructor(isValid: boolean, error: string) {
    this.errorMessage = error;
    this.isValid = isValid;
  }

  public isValid: boolean;

  public errorMessage: string;
}
