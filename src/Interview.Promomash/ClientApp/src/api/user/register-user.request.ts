export class RegisterUserRequest {

  public email: string;
  public password: string;
  public passwordConfirm: string;
  public cityId: number;
  public iAmAgree: boolean;
}
