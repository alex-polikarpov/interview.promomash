import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CheckEmailResponse } from './check-email.response';
import { Observable } from 'rxjs/Observable';
import { BaseApi } from '../base/base.api';
import {ApiResponse} from "../base/api.response";
import {RegisterUserRequest} from "./register-user.request";

@Injectable()
export class UserApi extends BaseApi {
  private _apiUrl: string;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    super(http, baseUrl);
    this._apiUrl = "api/user/";
  }

  public checkEmail(email: string): Promise<CheckEmailResponse> {
    return this.get(this._apiUrl + "checkemail?email=" + email, new CheckEmailResponse());
  }

  public register(email: string, password: string, passwordConfirm: string, cityId: number, userAgree: boolean): Promise<ApiResponse> {
    var args = new RegisterUserRequest();    
    args.cityId = cityId;
    args.email = email;
    args.password = password;
    args.passwordConfirm = passwordConfirm;
    args.iAmAgree = userAgree;

    return this.post(this._apiUrl + "register", args, new ApiResponse());
  }
}
