import { ApiResponse } from '../base/api.response';
import { CountryEntity } from "../../entities/country.entity";

export class GetCountriesResponse extends ApiResponse {
  public countries: CountryEntity[];
}
