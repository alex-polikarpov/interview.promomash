import { ApiResponse } from '../base/api.response';
import { CityEntity } from "../../entities/city.entity";

export class GetCitiesResponse extends ApiResponse {
  public cities: CityEntity[];
}
