export class ErrorsConstants {
  static required = "Fill in the field";

  static maxLength(length: number): string {
    return "Max length is " + length + " symbols";
  }

  static minLength(length: number): string {
    return "Min length is " + length + " symbols";
  }
}
