﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Interview.Promomash.Common;
using Interview.Promomash.Constants;

namespace Interview.Promomash.Requests.User
{
    public class CheckEmailRequest
    {
        [DisplayName(nameof(Email))]
        [Required(AllowEmptyStrings = false)]
        [MinLength(Core.Entities.User.EMAIL_MIN_LENGTH, ErrorMessage = ValidationErrors.MIN_LENGTH)]
        [MaxLength(Core.Entities.User.EMAIL_MAX_LENGTH, ErrorMessage = ValidationErrors.MAX_LENGTH)]
        [RegularExpression(RegexConstants.EMAIL, ErrorMessage = ValidationErrors.INVALID_EMAIL)]
        public string Email { get; set; }
    }
}
