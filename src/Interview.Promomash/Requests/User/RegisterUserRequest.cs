﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Interview.Promomash.Business;
using Interview.Promomash.Common;
using Interview.Promomash.Constants;

namespace Interview.Promomash.Requests.User
{
    public sealed class RegisterUserRequest
    {
        [DisplayName(nameof(Email))]
        [Required(AllowEmptyStrings = false)]
        [MinLength(Core.Entities.User.EMAIL_MIN_LENGTH, ErrorMessage = ValidationErrors.MIN_LENGTH)]
        [MaxLength(Core.Entities.User.EMAIL_MAX_LENGTH, ErrorMessage = ValidationErrors.MAX_LENGTH)]
        [RegularExpression(RegexConstants.EMAIL, ErrorMessage = ValidationErrors.INVALID_EMAIL)]
        public string Email { get; set; }

        [DisplayName(nameof(Password))]
        [Required(AllowEmptyStrings = false)]
        [MinLength(Core.Entities.User.PASSWORD_MIN_LENGTH, ErrorMessage = ValidationErrors.MIN_LENGTH)]
        [MaxLength(Core.Entities.User.PASSWORD_MAX_LENGTH, ErrorMessage = ValidationErrors.MAX_LENGTH)]
        [RegularExpression(RegexConstants.PASSWORD, ErrorMessage = ValidationErrors.INVALID_PASSWORD)]
        public string Password { get; set; }

        [DisplayName("Password confirmation")]
        [Compare(nameof(Password), ErrorMessage = ValidationErrors.PASSWRODS_NOT_EQUALS)]
        public string PasswordConfirm { get; set; }

        [DisplayName("Province")]
        [Range(1, Int32.MaxValue, ErrorMessage = ValidationErrors.CITY_NOT_FOUND)]
        public int CityId { get; set; }

        // ReSharper disable once InconsistentNaming
        [Range(typeof(bool), "true", "true", ErrorMessage = ValidationErrors.USER_NOT_AGREE)]
        public bool IAmAgree { get; set; }
    }
}
