﻿using System.Runtime.CompilerServices;
using Interview.Promomash.Common;
using Interview.Promomash.Core.Entities;
using Microsoft.EntityFrameworkCore;

[assembly: InternalsVisibleTo(InternalsVisibleConstants.API_RPOJECT_NAME)]
[assembly: InternalsVisibleTo(InternalsVisibleConstants.DEPENDENCY_PROJECT_NAME)]

namespace Interview.Promomash.Infrastructure.Data
{
    public class PromomashContext : DbContext
    {
        public PromomashContext(DbContextOptions<PromomashContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<City> Citites { get; set; }

        public DbSet<Country> Countries { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CountryTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CityTypeCounfiguration());
            modelBuilder.ApplyConfiguration(new UserTypeConfiguration());

            SeedData(modelBuilder);
        }

        private void SeedData(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Country>().HasData(
                new Country() {Id = 1, Title = "United States"},
                new Country() {Id = 2, Title = "Russia"});

            modelBuilder.Entity<City>().HasData(
                new City() {Id = 1, Title = "New York", CountryId = 1},
                new City() {Id = 2, Title = "Los Angeles", CountryId = 1},
                new City() {Id = 3, Title = "Chicago", CountryId = 1});

            modelBuilder.Entity<City>().HasData(
                new City() {Id = 4, Title = "Moscow", CountryId = 2},
                new City() {Id = 5, Title = "Saint Petersburg", CountryId = 2},
                new City() {Id = 6, Title = "Novosibirsk", CountryId = 2});
        }
    }
}
