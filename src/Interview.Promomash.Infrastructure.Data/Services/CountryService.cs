﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interview.Promomash.Core.Entities;
using Interview.Promomash.Core.Services;
using Microsoft.EntityFrameworkCore;

namespace Interview.Promomash.Infrastructure.Data
{
    internal class CountryService : BaseService, ICountryService
    {
        public CountryService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<IList<Country>> GetCountries()
        {
            var repo = UnitOfWork.GetRepository<Country>();
            return await repo.Query().AsNoTracking().OrderBy(x => x.Title).ToArrayAsync();
        }

        public async Task<IList<City>> GetCities(int countryId)
        {
            var repo = UnitOfWork.GetRepository<City>();
            return await repo.Query().AsNoTracking()
                .Where(x => x.CountryId == countryId)
                .OrderBy(x => x.Title)
                .ToArrayAsync();
        }

        public async Task<bool> CityIsExists(int cityId)
        {
            var repo = UnitOfWork.GetRepository<City>();
            return await repo.Query().AsNoTracking().AnyAsync(x => x.Id == cityId);
        }
    }
}
