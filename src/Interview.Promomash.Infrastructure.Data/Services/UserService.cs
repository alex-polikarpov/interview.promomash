﻿using System.Threading.Tasks;
using Interview.Promomash.Core.Entities;
using Interview.Promomash.Core.Services;
using Microsoft.EntityFrameworkCore;

namespace Interview.Promomash.Infrastructure.Data
{
    internal class UserService : BaseService, IUserService
    {
        public UserService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<bool> EmailIsUsed(string email)
        {
            var repo = UnitOfWork.GetRepository<User>();
            var lowerEmail = email?.ToLowerInvariant();
            return await repo.Query().AsNoTracking().AnyAsync(x => x.Email == lowerEmail);
        }
    }
}
