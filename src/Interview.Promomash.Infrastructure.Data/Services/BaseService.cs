﻿using Interview.Promomash.Core.Services;

namespace Interview.Promomash.Infrastructure.Data
{
    internal abstract class BaseService
    {
        protected BaseService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        protected IUnitOfWork UnitOfWork { get; }
    }
}
