﻿using System.Linq;
using Interview.Promomash.Core.Entities;
using Interview.Promomash.Core.Services;
using Microsoft.EntityFrameworkCore;

namespace Interview.Promomash.Infrastructure.Data
{
    /// <summary>
    /// Implementation of repository by entity framework
    /// </summary>
    internal sealed class EfRepository<TEntity> : Repository<TEntity> where TEntity : class, IEntity
    {
        private readonly DbContext _dbContext;
        private DbSet<TEntity> _dbSet;

        public EfRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = dbContext.Set<TEntity>();
        }

        internal override IQueryable<TEntity> Query()
        {
            return _dbSet;
        }

        public override TEntity Add(TEntity entity)
        {
            return _dbSet.Add(entity)?.Entity;           
        }

        public override void Remove(TEntity entity)
        {
            Attach(entity);
            _dbSet.Remove(entity);
        }

        public override TEntity Update(TEntity entity)
        {
            Attach(entity);
            _dbContext.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        private void Attach(TEntity entity)
        {
            if (_dbContext.Entry(entity).State == EntityState.Detached)
            {
                _dbSet.Attach(entity);
            }
        }
    }
}
