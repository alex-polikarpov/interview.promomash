﻿namespace Interview.Promomash.Business
{
    public abstract class BaseCommandResult : ICommandResult
    {
        public virtual bool IsSuccess { get; set; }

        public virtual string ErrorMessage { get; set; }

        public virtual int ErrorCode { get; set; }
    }
}
