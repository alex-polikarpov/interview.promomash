﻿using System.Threading.Tasks;

namespace Interview.Promomash.Business
{
    /// <summary>
    /// Business command interfac. Performs some work.
    /// WARNING!!! command must have a unique combination of args and result (see settings for IoC container)
    /// </summary>
    /// <typeparam name="TArguments">Arguments type for command</typeparam>
    /// <typeparam name="TResult">Result type for command</typeparam>    
    public interface ICommand<TArguments, TResult>
        where TArguments : class, ICommandArguments
        where TResult : class, ICommandResult, new()
    {
        /// <summary>
        /// Execute command
        /// </summary>
        Task<TResult> Execute(TArguments arguments);
    }
}
