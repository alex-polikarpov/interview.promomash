﻿using Interview.Promomash.Core.Entities;
using System.Linq;
using System.Runtime.CompilerServices;
using Interview.Promomash.Common;

[assembly: InternalsVisibleTo(InternalsVisibleConstants.ORM_PROJECT_NAME)]

namespace Interview.Promomash.Core.Services
{
    /// <summary>
    /// Repository for working with entities
    /// </summary>
    /// <typeparam name="TEntity">Entity type</typeparam>
    public abstract class Repository<TEntity> where TEntity : class, IEntity
    {
        /// <summary>
        /// Returns IQueryable expression
        /// </summary>
        internal abstract IQueryable<TEntity> Query();

        /// <summary>
        /// Add new entity to repository (without saving)
        /// </summary>
        public abstract TEntity Add(TEntity entity);

        /// <summary>
        /// Delete entity from repository (without saving)
        /// </summary>
        public abstract void Remove(TEntity entity);

        /// <summary>
        /// Update entity in repository (without saving)
        /// </summary>
        public abstract TEntity Update(TEntity entity);
    }
}
