﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Interview.Promomash.Core.Entities;

namespace Interview.Promomash.Core.Services
{
    public interface ICountryService
    {
        /// <summary>
        /// Returns all countries ordered by name
        /// </summary>
        Task<IList<Country>> GetCountries();

        /// <summary>
        /// Returns all country cities ordered by name
        /// </summary>
        Task<IList<City>> GetCities(int countryId);

        /// <summary>
        /// Returns true if city is exists
        /// </summary>
        Task<bool> CityIsExists(int cityId);
    }
}
