﻿using System.Threading.Tasks;

namespace Interview.Promomash.Core.Services
{
    public interface IUserService
    {
        /// <summary>
        /// Returns false if email is not used
        /// </summary>
        Task<bool> EmailIsUsed(string email);
    }
}
